#ifndef LOGGER_H
#define LOGGER_H

#include <fstream>

class Logger
{
public:
    Logger();
    Logger& operator<<(std::string str);
private:
    std::fstream logFile;
};

#endif // LOGGER_H
