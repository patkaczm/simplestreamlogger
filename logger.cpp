#include "logger.h"

#include <ios>

Logger::Logger()
{
    logFile = std::fstream ("logger.txt", std::ios::trunc | std::ios::out);
}

Logger& Logger::operator<<(std::string str) {
    logFile << str;
    logFile.flush();
    return *this;
}
